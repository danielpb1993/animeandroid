package com.example.anime;

import android.content.Intent;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends Fragment {
    private static final String urlUpdateUser = Server.IP + Server.HTDOCS + Server.UPDATEUSER;
    RequestQueue requestQueue;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;

    public UserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserFragment newInstance(String param1, String param2, String param3, String param4) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_user, container, false);
        EditText inputEditTextUserName = view.findViewById(R.id.inputEditUserName);
        TextView textViewEmail = view.findViewById(R.id.textViewEmailUser);
        EditText inputEditTextPassowrd = view.findViewById(R.id.inputEditUserPAssword);
        EditText inputEditTextPhone = view.findViewById(R.id.inputEditUserPhone);

        Button btUpdateUser = view.findViewById(R.id.btUpdateUser);
        Button btUDeleteUser = view.findViewById(R.id.btDeleteUser);

        requestQueue = Volley.newRequestQueue(getContext());


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);

            inputEditTextUserName.setText(mParam1);
            textViewEmail.setText(mParam2);
            inputEditTextPassowrd.setText(mParam3);
            inputEditTextPhone.setText(mParam4);

        }
        btUpdateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameV = inputEditTextUserName.getText().toString().trim();
                String passwordV = inputEditTextPassowrd.getText().toString().trim();
                String phoneV = inputEditTextPhone.getText().toString().trim();
                if (!nameV.isEmpty() & !passwordV.isEmpty() && !phoneV.isEmpty()) {
                    updateUser(nameV, passwordV, phoneV);
                } else {
                    Toast.makeText(getContext(), "Enter data", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    private void updateUser(final String nameV, final String passwordV, final String phoneV) {
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                urlUpdateUser,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getContext(), "User updated", Toast.LENGTH_SHORT).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Error updating user", Toast.LENGTH_SHORT).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", nameV);
                params.put("password", passwordV);
                params.put("phone", phoneV);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}