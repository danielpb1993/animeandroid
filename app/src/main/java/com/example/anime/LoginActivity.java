package com.example.anime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    TextInputEditText email;
    TextInputEditText password;
    Button btLoginLogin;
    Button btSignupLogin;

    RequestQueue requestQueue;
    private static final String urlLogin = Server.IP + Server.HTDOCS + Server.LOGINUSER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.inputEditMailLogin);
        password = findViewById(R.id.inputEditPasswordLogin);
        btLoginLogin = findViewById(R.id.btLoginLogin);
        btSignupLogin = findViewById(R.id.btSignupLogin);

        btSignupLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });

        btLoginLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailV = email.getText().toString().trim();
                String passwordV = password.getText().toString().trim();
                if (!emailV.isEmpty() && !passwordV.isEmpty()) {
                    loginUser(emailV, passwordV);
                } else {
                    Toast.makeText(LoginActivity.this, "Enter data", Toast.LENGTH_SHORT).show();
                }
            }
        });
        requestQueue = Volley.newRequestQueue(this);
    }


    private void loginUser(String emailV, String passwordV) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                urlLogin + "?email=" + emailV + "&password=" + passwordV,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String nameV, emailV, passwordV, phoneV;
                        try {
                            nameV = response.getString("name");
                            emailV = response.getString("email");
                            passwordV = response.getString("password");
                            phoneV = response.getString("phone");
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra("name", nameV);
                            intent.putExtra("email", emailV);
                            intent.putExtra("password", passwordV);
                            intent.putExtra("phone", phoneV);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, "Error loging in", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }
}