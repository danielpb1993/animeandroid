package com.example.anime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity {
    private TextInputEditText name;
    private TextInputEditText email;
    private TextInputEditText password;
    private TextInputEditText phone;
    Button btLogin;
    Button btSignup;

    RequestQueue requestQueue;
    private static final String urlCreateUser = Server.IP + Server.HTDOCS + Server.CREATEUSER;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        name = findViewById(R.id.inputEditNameSignup);
        email = findViewById(R.id.inputEditMailSignup);
        password = findViewById(R.id.inputEditPasswordSignup);
        phone = findViewById(R.id.inputEditPhoneSignup);
        btLogin = findViewById(R.id.btLogin);
        btSignup = findViewById(R.id.btSignUp);

        requestQueue = Volley.newRequestQueue(this);

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        btSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(SignupActivity.this, MainActivity.class);
//                startActivity(intent);
                String nameV = name.getText().toString().trim();
                String emailV = email.getText().toString().trim();
                String passwordV = password.getText().toString().trim();
                String phoneV = phone.getText().toString().trim();
                if (!nameV.isEmpty() && !emailV.isEmpty() && !passwordV.isEmpty() && !phoneV.isEmpty()) {
                    createUser(nameV, emailV, passwordV, phoneV);
                } else {
                    Toast.makeText(SignupActivity.this, "Enter data", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void createUser(final String nameV, final String emailV, final String passwordV, final String phoneV) {
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                urlCreateUser,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(SignupActivity.this, "User created", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                        intent.putExtra("name", nameV);
                        intent.putExtra("email", emailV);
                        intent.putExtra("password", passwordV);
                        intent.putExtra("phone", phoneV);
                        startActivity(intent);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SignupActivity.this, "Error creating user", Toast.LENGTH_SHORT).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", nameV);
                params.put("email", emailV);
                params.put("password", passwordV);
                params.put("phone", phoneV);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}