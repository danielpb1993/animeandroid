package com.example.anime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    private FrameLayout mMainFrame;
    private ListFragment listFragment;
    private UserFragment userFragment;
    private FavoritesFragment favoritesFragment;
    private ArrayList<Anime> animes = new ArrayList<>();

    String JSON_URL = "";
    String JSON_NAME = "";
    String JSON_MAIL = "";
    String JSON_PASSWORD = "";
    String JSON_PHONE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        JSON_URL = "https://joanseculi.com/edt69/animes3.php?email=";
        Intent intent = getIntent();

        JSON_NAME = intent.getStringExtra("name");
        JSON_MAIL = intent.getStringExtra("email");
        JSON_PASSWORD = intent.getStringExtra("password");
        JSON_PHONE = intent.getStringExtra("phone");

        toolbar = findViewById(R.id.toolbar);
        mMainFrame = findViewById(R.id.main_frame);

        setSupportActionBar(toolbar);

        getAnimes();

        userFragment = UserFragment.newInstance(JSON_NAME, JSON_MAIL, JSON_PASSWORD, JSON_PHONE);
        favoritesFragment = new FavoritesFragment();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.list:
                setFragment(listFragment);
                return true;
            case R.id.user:
                setFragment(userFragment);
                return true;
            case R.id.likes:
                setFragment(favoritesFragment);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }

    private void getAnimes() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL + JSON_MAIL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("animes");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject animeObject = (JSONObject) jsonArray.get(i);
                                Anime anime = new Anime();
                                anime.setId(Integer.parseInt(animeObject.getString("id")));
                                anime.setName(animeObject.getString("name"));
                                anime.setDescription(animeObject.getString("description"));
                                anime.setType(animeObject.getString("type"));
                                anime.setImage(Server.IP + animeObject.getString("image"));
                                anime.setYear(Integer.parseInt(animeObject.getString("year")));
                                anime.setFavorite(animeObject.getString("favorite"));
                                animes.add(anime);
                            }
                            listFragment = ListFragment.newInstance(animes);
                            setFragment(listFragment);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }
}