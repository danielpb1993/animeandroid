package com.example.anime;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AnimeAdapter extends RecyclerView.Adapter<AnimeAdapter.MyViewHolder> {
    private ArrayList<Anime> mAnimes;
    private Context mContext;

    public AnimeAdapter(ArrayList<Anime> mAnimes, Context mContext) {
        this.mAnimes = mAnimes;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.anime_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(mAnimes.get(position).getImage())
                .fit()
                .centerCrop()
                .into(holder.imageViewList);
        holder.animeNameList.setText(mAnimes.get(position).getName());
        holder.animeDescList.setText(mAnimes.get(position).getDescription());
        holder.animeYearList.setText(String.valueOf(mAnimes.get(position).getYear()));
        holder.animeTypeList.setText(mAnimes.get(position).getType());
        if (mAnimes.get(position).getFavorite().equals("null")) {
            holder.animeLikeList.setImageResource(R.drawable.ic_dontlike);
        } else {
            holder.animeLikeList.setImageResource(R.drawable.ic_like);
        }

    }

    @Override
    public int getItemCount() {
        return mAnimes.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewList;
        private TextView animeNameList;
        private TextView animeDescList;
        private TextView animeYearList;
        private TextView animeTypeList;
        private ImageView animeLikeList;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewList = itemView.findViewById(R.id.imageViewList);
            animeNameList = itemView.findViewById(R.id.animeNameList);
            animeDescList = itemView.findViewById(R.id.animeDescList);
            animeYearList = itemView.findViewById(R.id.animeYearList);
            animeTypeList = itemView.findViewById(R.id.animeTypeList);
            animeLikeList = itemView.findViewById(R.id.animeLikeList);
        }
    }
}
